from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.urls import path
from django.views.generic import TemplateView

from .urls_static import urlpatterns as urlpatterns_s

urlpatterns = [
    path('', TemplateView.as_view(template_name='index.html')),
    path('about/', TemplateView.as_view(template_name='index.html')),

    path('admin/', admin.site.urls),
]
urlpatterns += urlpatterns_s

if settings.DEBUG:
    import debug_toolbar

    urlpatterns.insert(0, url(r'^__debug__/', include(debug_toolbar.urls)))
