from .settings import *

DEBUG = False
ALLOWED_HOSTS = ['*']

WEBPACK_LOADER = {
    'DEFAULT': {
        'BUNDLE_DIR_NAME': '',
        'STATS_FILE': os.path.join(os.path.dirname(BASE_DIR), 'frontend/webpack-stats-prod.json'),
    }
}
VUE_ROOT = os.path.join(os.path.join(os.path.dirname(BASE_DIR), 'frontend'), 'dist')
