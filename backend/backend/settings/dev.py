from .settings import *

DEBUG = True
ALLOWED_HOSTS = ['127.0.0.1', 'localhost']

WEBPACK_LOADER = {
    'DEFAULT': {
        'BUNDLE_DIR_NAME': '',
        'STATS_FILE': os.path.join(os.path.dirname(BASE_DIR), 'frontend/webpack-stats.json'),
    }
}
VUE_ROOT = os.path.join(os.path.join(os.path.dirname(BASE_DIR), 'frontend'), 'static')

MIDDLEWARE.append('debug_toolbar.middleware.DebugToolbarMiddleware')
INSTALLED_APPS.append('debug_toolbar')
INTERNAL_IPS = ('127.0.0.1', 'localhost')
