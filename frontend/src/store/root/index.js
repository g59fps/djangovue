import root_state from './state'
import * as root_getters from './getters'
import * as root_mutations from './mutations'
import * as root_actions from './actions'

export const state = root_state;
export const getters = root_getters;
export const mutations = root_mutations;
export const actions = root_actions;
