import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(Vuex)
Vue.use(VueAxios, Axios)

import account from './account'
import {state, getters, mutations, actions} from './root'

export default new Vuex.Store({
  modules: {
    account,

  },
  state,
  getters,
  mutations,
  actions,
})
